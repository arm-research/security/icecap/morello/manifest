sel4test-manifest for Morello Experiments
=========================================
This is a fork of the sel4test-manifest from https://github.com/seL4/sel4test-manifest.git.

We are using it to test out the small morello proof of concepts with seL4.

# Setting up a dev environment Ubuntu 20.04 LTS(only)

## Morello Toolchain
```bash
git clone  git@github.com:CTSRD-CHERI/cheribuild.git
cd cheribuild
sudo apt install libpixman-1-dev samba -y
./cheribuild.py run-morello-purecap -d
[... this will take a while ...]
Step out of the booted bsd

# Preferably add it to you bash/zsh src file.
export PATH=~/cheri/output/morello-sdk/bin:$PATH

```


Other misc packages
```bash
pip3 uninstall libarchive
pip3 install libarchive-c
```

## sel4 test

Get the source
```bash
mkdir sel4test
cd sel4test
repo init -u git@gitlab.com:icecap-project/morello/manifest.git
repo sync
```


Build it.
```bash
mkdir build-cheri; cd build-cheri
../init-build.sh -DTRIPLE=aarch64-linux-gnu -DAARCH64=TRUE  -DPLATFORM=qemu-arm-virt -DSIMULATION=TRUE
ninja 
./simulate -b ~/cheri/output/sdk/bin/qemu-system-morello -c morello
```

To exit qemu, press `Ctrl-a, then x`. The text string `All is well in the universe` indicates a successful build.

# Making changes
The git repos are setup with https remotes, which does not let us push code.
So use a dev.xml manifest in which the remotes are setup as ssh based git repos.

```bash
mkdir sel4test
cd sel4test
repo init -u git@gitlab.com:arm-research/security/icecap/morello/manifest.git -m dev.xml
repo sync

# Checkout the correct branch
git -C projects/seL4_libs/ checkout morello
git -C projects/sel4test checkout morello
git -C kernel checkout morello
git -C tools/seL4 checkout morello

TODO(sid): Look into if this be done by repo tool automatically?
```

# Debugging

## GDB
To debug using GDB we recommend using the gdb provided by cheribuild. 
It can be built as follows:
```bash
cd <cheribuild-dir>
./cheribuild.py gdb-native
```

To debug, start the simulation with some extra args as follows:

```bash
cd build
ninja 
./simulate -b ~/cheri/output/morello-sdk/bin/qemu-system-morello \
           -c morello --extra-qemu-args="-S -s -D misc.log"
```

In another terminal, start gdb and connect to the debugged instance as follows:
```bash
 ~/cheri/output/sdk/bin/gdb \
                    kernel/kernel.elf
                    -ex "target remote :1234"
```
If you are debugging the `sel4test-driver`, the command changes to:

```bash
 ~/cheri/output/sdk/bin/gdb 
      apps/sel4test-driver/sel4test-driver 
      -ex "target remote :1234"
 ```

If you are debugging the `sel4test-test`, the command changes to:

```bash
 ~/cheri/output/sdk/bin/gdb 
      apps/sel4test-driver/sel4test-test/sel4test-test 
      -ex "target remote :1234"
 ```

## Objdump
Use the `--mattr=+morello` with objdump to see the morello instructions. 
Without this option, the morello instructions (or instructions with morello args) appear as \<unknown\>.

```bash
~/cheri/output/morello-sdk/bin/objdump -D  --mattr=+morello  <binary>
```
Source: https://git.morello-project.org/morello/llvm-project-releases/-/blob/morello/release-docs-1.0/UserGuide.pdf

## Instruction Logging

If you want to log all the instructions executed by the CPU, add
`--extra-qemu-args "-D inst.log -d in_asm,exec,nochain" ` to the simulate command.

```bash
./simulate -b ~/cheri/output/morello-sdk/bin/qemu-system-morello -c morello --extra-qemu-args "-D inst.log -d in_asm,exec,nochain"
```

### Selective logging

The above command can be very slow and generate very large log files.
For instance, just the boot process of seL4 generates around 5GB of logs.

A neat trick is to use gdb to set a breakpoint. Then on the Qemu
console do `Ctrl-a then c` to drop to the console and run `log  in_asm,exec,nochain`
on the console. Alternatively, you can do `monitor log in_asm,exec,nochain` from gdb 
directly. Both of these methods will print the trace on the Qemu console.


## Decoding Capabilities
Ideally, we would have like gdb to show us all the fields of a capability, but the current version of 
cheribuild'd gdb shows the cap details for binary build with purecap flags.

There is a web-interface available at https://www.morello-project.org/capinfo to decipher the fields if you have a 129 binary number.
But even getting a 129 binary number is not possible right now as gdb only prints 64 bits even for cap registers like `pcc`.
