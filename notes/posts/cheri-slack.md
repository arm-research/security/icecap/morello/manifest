Greetings cheri folks,
My name is Sid Agrawal and I graduate student at UBC and summer Intern at ARM.
We are trying to figure out how we can use CHERI-morello with seL4. Before getting to interesting bits 
about software Vs. Hardware Capabilities, we are trying to ascertain that the we can call morello instructions 
in seL4 and that is raises the right exceptions. 

Here is what we have tried so far:
1. Call "LDUR X1, [C1] from a user task. This lead to an exception where `esr_el1` set to 0xa6000000. As per section of 3.3.25 of [1] it meant that an exception was raised since a morello instruction was executed. 

2. Next we read cpacr_el1 and found that cpacr_el1.cen was set to 0b00 i.e. raise exception on every instruction.

3. We decided to change  `cpacr_el1.cen` to 0b11 so that every morello instruction will not cause exception and instead we will see some type of capability fault. For instance, with the above LDUR instruction I was hoping to see a `invalid cap fault` or something similar. Since cpacr_el1 can only changed in EL1 mode, we added code in the kernel right before it jumps to user mode to change the value of `cpacr_el1.cen` to 0b11. [2]

4. Unfortunately, when we do this, the user-mode process never starts. 

My questions is:
- Where is the right place in the boot up process to set cpacr_el1.cen to 0b11. Is that even the right thing to do? Our code is setting it at [2]


Our environment is Ubuntu 20.04. The clang compiler is from `cheribuild.py` and the qemu is `qemu-system-morello`. Complete details on setup at available at [3]

[1] Arm® Architecture Reference Manual Supplement Morello for A-profile Architecture

[2] https://gitlab.com/arm-research/security/icecap/seL4/-/blob/40a2f5e367791a374a89cc369522d0fe9e7c734f/src/arch/arm/kernel/boot.c#L241

[3] https://gitlab.com/arm-research/security/icecap/morello/manifest